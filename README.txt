CALAIS

INTRO
=====

This module integrates Drupal with the Calais Web-Service. The web
service allows automated content term-extraction and tagging. It also 
provides an API in which any contributed module can interact with Calais.


INSTALLATION
============

1) Place this module directory into your Drupal modules directory.

2) Enable the Calais API, and Calais module in Drupal, at:
   administration -> site configuration -> modules (admin/build/modules)

4) Add Calais API Key and tune other settings at:
   administration -> site configuration -> modules -> Calais
   (admin/settings/calais-api)


CREDITS
========
Written by
  - Irakli Nadareshvili <irakli at phase2technology dot com>
  - Frank Febbraro <frank at phase2technology dot com>
  
Sponsored by
  - Phase2 Technology <http://www.phase2technology.com>
  - ThomsonReuters <http://www.thomsonreuters.com/>
  
